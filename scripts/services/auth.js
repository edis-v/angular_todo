(function(){

    var auth = function($http){
        this.login = function(record){
            var request = $http({
                method: 'post',
                url: 'http://team-to-do.herokuapp.com/api/v1/access',
                data: record
            });
            return request;
        };
    };


    App.service('auth',auth);
}());