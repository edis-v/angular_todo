(function(){

    var crud_service = function($http){

        this.get_me = function(record){
            var request = $http({
                method: 'get',
                url: 'http://team-to-do.herokuapp.com/api/v1/'+record
            });
            return request;
        };

        this.post_me = function(source, data) {
            var request = $http({
                method: 'post',
                url: 'http://team-to-do.herokuapp.com/api/v1/'+source,
                data: data
            });
            return request;
        };

        this.delete_me = function(source){
            var request = $http({
                method: 'delete',
                url: 'http://team-to-do.herokuapp.com/api/v1/'+source
            });
            return request;
        };

        this.put_me = function(source,data){
            var request = $http({
                method: 'put',
                url: 'http://team-to-do.herokuapp.com/api/v1/'+source,
                data: data
            });
            return request;
        };

    };

    App.service('crud_service',crud_service);
}());