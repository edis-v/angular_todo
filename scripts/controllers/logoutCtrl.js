(function(){

    var logoutCtrl = function(crud_service, $location){
        logOut();

        function logOut(){
            var user = window.localStorage.getItem('current_user_id');
            if (user != undefined){
                var promise = crud_service.delete_me('access/'+user);
                promise.then(function(response){
                    window.localStorage.removeItem('current_user_id');
                    window.localStorage.removeItem('token');
                    $location.path('/login');
                },function(response){});
            }
        }
    };

    App.controller('logoutCtrl',logoutCtrl);
}());
