(function(){

    var modalCtrlList = function($scope, $modalInstance,crud_service,$location, list){
        $scope.modalList = angular.copy(list);

        $scope.updateList = function(){
            $scope.modalList.user_id = window.localStorage.getItem('current_user_id');
            var data = {'list': $scope.modalList};
            var promise = crud_service.put_me('lists/'+$scope.modalList.id,data);
            promise.then(function (response) {
            list =  $scope.modalList;
                $modalInstance.close(response);
            }, function (response) {
                alert('An error occurred');
                $modalInstance.close();
            });

        };



        $scope.createList = function(){
            $scope.list.user_id = window.localStorage.getItem('current_user_id');
            var data = {'list': $scope.list};
            var promise = crud_service.post_me('lists',data);
            promise.then(function (response) {
                $modalInstance.close(response);
            }, function (response) {
                alert('An error occurred');
                $modalInstance.close();
            });
        };
    };


    App.controller('modalCtrlList',modalCtrlList);
}());