(function(){



    var taskCtrl = function($scope, crud_service, $stateParams, $modal){


        getTasks();

        $scope.show_create = function(){
            var promise = crud_service.get_me('lists');
            promise.then(
                function(response){
                    var modalInstance = $modal.open({
                        templateUrl: 'views/create_task.html',
                        controller: 'modalCtrlTask',
                        resolve: {
                            lists: function () {
                                return response.data;
                            },
                            task: function(){
                                var ret = {'list_id' : $stateParams.id};
                                return ret;
                            }
                        }
                    });
                    modalInstance.result.then(
                        function(response){
                            $scope.tasks.push(response);
                        },
                        function(response){
                            //not submitted
                        });
                },
                function(response){
                    alert('Error while loading lists');
            });

        };

        $scope.show_update = function(task){
            var promise = crud_service.get_me('lists');
            promise.then(
                function(response){
                    var modalInstance = $modal.open({
                        templateUrl: 'views/update_task.html',
                        controller: 'modalCtrlTask',
                        resolve: {
                            lists: function () {
                                return response.data;
                            },
                            task: function() {
                                return task;
                            }
                        }
                    });
                    modalInstance.result.then(
                        function(response){
                            getTasks();
                        },
                        function(response){
                          //not submitted
                        });
                },function(response){
                    alert('Error while loading lists');
                });
        };

        $scope.remove = function(task_id){
            var promise = crud_service.delete_me('tasks/'+task_id);
            promise.then(
                function(response){
                    getTasks();
                },
                function(response){
                    alert('Error');
            });
        };


        $scope.change_state = function(task){
            var promise = crud_service.put_me('tasks/'+task.id, task);
            promise.then(
                function (response) {
                    //success
                },
                function (response) {
                    alert('Error');
            });
        };


        function getTasks(){
            var promise = crud_service.get_me('lists/'+$stateParams.id);
            promise.then(
                function(response){
                    $scope.list_name = response.data.name;
                    $scope.tasks =response.data.tasks;
                    if($scope.tasks.length==0){
                        $scope.msg = 'Empty';
                    } else $scope.msg = "";
                },
                function(response){
                    alert('Error');
                });
        }

    };

    App.controller('taskCtrl',taskCtrl);

}());