(function(){


    var registrationCtrl = function($scope, crud_service, $location){
        $scope.register = function(){
        var data = {'user': $scope.user};
            var promise = crud_service.post_me('users',data);
            promise.then(
                function(response){
                    //registration success
                    $location.path('/login');
                },
                function(response){
                    alert('Error while trying to register');
            });
        };
    };

    App.controller('registrationCtrl',registrationCtrl);

}());