(function(){

    var modalCtrlTask = function($scope, $modalInstance,crud_service, lists, task){
        $scope.lists = lists;
        //$scope.task = task;
        $scope.modalTask = angular.copy(task);

        var promise = crud_service.get_me('lists/'+task.list_id);
        promise.then(
            function(response){
                $scope.selectedList = response.data;
            },
            function(response){
                alert('An error occurred while reading lists');
        });

        $scope.createTask = function () {
            $scope.task.list_id = $scope.selectedList.id;
            $scope.task.state = false;
            var data = {'task': $scope.task};
            var promise = crud_service.post_me('tasks',data);
            promise.then(
                function(response){
                    $modalInstance.close(response.data);
                },
                function (response) {
                    alert('Error while creating task');
            });
        };

        $scope.updateTask = function () {
            //$scope.task.list_id = $scope.selectedList.id;
            $scope.modalTask.list_id = $scope.selectedList.id;
            var data = {'task': $scope.modalTask};
            var promise = crud_service.put_me('tasks/'+$scope.modalTask.id,data);
            promise.then(
                function(response){
                    task = $scope.modalTask;
                    $modalInstance.close(response.data);
                },
                function (response) {
                    alert('Error while updating task');
                    $modalInstance.close();
            });
        }
    };

    App.controller('modalCtrlTask',modalCtrlTask);


}());