(function(){

    var authCtrl = function($scope, auth, $location){

        $scope.logUser = function(){
            var user = {'access' : $scope.user};
            var promise = auth.login(user);
            promise.then(
                function(response){
                    //success
                    window.localStorage.setItem('token',response.data.auth_token);
                    window.localStorage.setItem('current_user_id',response.data.id);
                    $location.path('/lists');
                },
                function(response){
                    //error
                    alert("error");
            });
        };
    };

    App.controller('authCtrl',authCtrl);
}());