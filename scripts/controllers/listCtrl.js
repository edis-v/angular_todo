(function(){

    var listCtrl = function($scope,crud_service, $location, $modal){

        getLists();

        $scope.remove = function(list_id){
            console.log('ID liste za brisanje je',list_id);
            var promise = crud_service.delete_me('lists/'+list_id);
            promise.then(
                function(response){
                    getLists();
                },
                function(response){
                    alert("Error");
            });
        };

        $scope.show_create = function(){
            var modalInstance = $modal.open({
                templateUrl: 'views/modal_list_create.html',
                controller: 'modalCtrlList',
                resolve: {
                    list: function () {}
                }
            });

            modalInstance.result.then(
                function (response) {
                    getLists();
                },
                function (response) {
                    //error
            });

        };

        $scope.show_edit = function(list){
            var modalInstance = $modal.open({
                templateUrl: 'views/modal_list_update.html',
                controller: 'modalCtrlList',
                resolve: {
                    list: function () {
                        return list;
                    }
                }
            });
            modalInstance.result.then(
                function (response) {
                    getLists();
                },
                function (response) {
                    //error
            });
        };




        function getLists(){
            var promise = crud_service.get_me('lists');
            promise.then(function(response){
                //success
                $scope.lists=response.data;
            },function(){
                //error
                console.log('error');
            });
        }

    };

    App.controller('listCtrl',listCtrl);
}());