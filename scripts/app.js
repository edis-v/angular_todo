var App = angular.module('myApp',['ui.router','ui.bootstrap']);

App.config(function($stateProvider, $urlRouterProvider,  $httpProvider){

    $urlRouterProvider.otherwise('/index');

    $stateProvider
        .state('index', {
            url: '/index',
            template: '',
            controller: 'testCtrl'
        })
        .state('registration', {
            url: '/registration',
            templateUrl: 'views/registration.html',
            controller: 'registrationCtrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'authCtrl'
        })
        .state('logout', {
            url: '/logout',
            templateUrl: 'views/login.html',
            controller: 'logoutCtrl'
        })
        .state('lists',{
            url: '/lists',
            templateUrl: 'views/list.html',
            controller: 'listCtrl'
        })
        .state('lists.task',{
            url: '/lists/:id',
            templateUrl: 'views/specList.html',
            controller: 'taskCtrl'
        });




    $httpProvider.interceptors.push('intercepter');

});

//on every route change check if user is logged in
//logged in user can't go to login or registration page
//logged out user can't go to lists, task page
App.run(["$rootScope", function($rootScope){
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
            if (window.localStorage.getItem('token')!= undefined) {
                $rootScope.show = true;
                if(toState.name == 'login' || toState.name == 'registration') {
                    event.preventDefault();
                }
            } else if (window.localStorage.getItem('token')== undefined) {
                $rootScope.show = false;
                if(toState.name == 'lists' || toState.name == 'logout' || toState.name == 'lists.task') {
                    event.preventDefault();
                }
            }
        });

}]);