var taskDirective = function(){
    return {
        restrict: 'E',
        replace: true,
        scope: {
            //state: '=',
            tas: '=',
            changes: '&',
            show: '&',
            rem: '&'
        },
        templateUrl: 'views/task_directive.html'
    }
};

App.directive('taskDirective',taskDirective);
